package com.example.quiz

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.quiz.utils.MockRemote
import kotlinx.android.synthetic.main.activity_results.*

class ResultsActivity : AppCompatActivity() {

    private var total = 0
    private var correctCount = 0
    private var wrongCount = 0
    private var unsolvedCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)
        val bundle = intent.extras
        bundle?.let {
            total = bundle.getInt("TOTAL")
            correctCount = bundle.getInt("CORRECT")
            wrongCount = bundle.getInt("WRONG")
            unsolvedCount = bundle.getInt("UNSOLVED")
        }
        txtTotal.text = total.toString()
        txtCorrect.text = correctCount.toString()
        txtWrong.text = wrongCount.toString()
        txtUnsolved.text = unsolvedCount.toString()

        again.setOnClickListener {
            startActivity(Intent(this, QuestionsActivity::class.java))
            finish()
        }

        logout.setOnClickListener {
            MockRemote.setDefaults("token", "", this)
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

    }
}
