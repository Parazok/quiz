package com.example.quiz.utils

import android.content.Context
import android.preference.PreferenceManager
import com.example.quiz.model.Question
import com.example.quiz.model.QuestionEntity


class MockRemote {
    companion object{
//        private fun createQuestions(): List<QuestionEntity> = listOf(
//            QuestionEntity(
//                text = "What is Android Jetpack?",
//                answers = listOf("all of these", "tools", "documentation", "libraries"),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Base class for Layout?",
//                answers = listOf("ViewGroup", "ViewSet", "ViewCollection", "ViewRoot"),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Layout for complex Screens?",
//                answers = listOf("ConstraintLayout", "GridLayout", "LinearLayout", "FrameLayout"),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Pushing structured data into a Layout?",
//                answers = listOf("Data Binding", "Data Pushing", "Set Text", "OnClick"),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Inflate layout in fragments?",
//                answers = listOf("onCreateView", "onActivityCreated", "onCreateLayout", "onInflateLayout"),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Build system for Android?",
//                answers = listOf("Gradle", "Graddle", "Grodle", "Groyle"),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Android vector format?",
//                answers = listOf(
//                    "VectorDrawable",
//                    "AndroidVectorDrawable",
//                    "DrawableVector",
//                    "AndroidVector"
//                ),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Android Navigation Component?",
//                answers = listOf("NavController", "NavCentral", "NavMaster", "NavSwitcher"),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Registers app with launcher?",
//                answers = listOf("intent-filter", "app-registry", "launcher-registry", "app-launcher"),
//                answer = 1
//            ),
//            QuestionEntity(
//                text = "Mark a layout for Data Binding?",
//                answers = listOf("<layout>", "<binding>", "<data-binding>", "<dbinding>"),
//                answer = 1
//            )
//        )

        var token = ""

         fun mapQuestionEntityToQuestion(questionEntities: List<QuestionEntity>?): MutableList<Question>{
            val questions = mutableListOf<Question>()
             if (questionEntities != null) {
                 for(questionEntity in questionEntities){
                     val question = Question(
                         id = questionEntity.id,
                         text = questionEntity.text,
                         answers = listOf(
                             questionEntity.option1, questionEntity.option2, questionEntity.option3,
                             questionEntity.option4
                         ),
                         correctAnswer = questionEntity.answer
                     )
                     questions.add(question)
                 }
             }
            return questions
        }

        fun setDefaults(
            key: String?,
            value: String?,
            context: Context?
        ) {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(context)
            val editor = preferences.edit()
            editor.putString(key, value)
            editor.commit()
        }

        fun getDefaults(key: String?, context: Context?): String? {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(key, null)
        }

//        fun qetAllQuestions() : MutableList<Question>{
//            return mapQuestionEntityToQuestion(createQuestions())
//        }
    }
}
