package com.example.quiz.model

data class Question(
    val id: Int,
    val text: String,
    val answers: List<String>,
    val correctAnswer: Int,
    val selectedAnswer: Int? = null,
    val remainingTime: Int = 60
)