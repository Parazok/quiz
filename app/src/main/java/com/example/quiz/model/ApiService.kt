package com.example.quiz.model

import com.example.quiz.model.QuestionResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiService {


    @POST("auth/")
    fun authentication(
        @Body authRequest: AuthRequest
    ) : Call<AuthResponse>

    @POST("get-questions/")
    fun getQuestions(@Body questionRequest: QuestionRequest): Call<QuestionResponse>

    @POST("exam/")
    fun sendExam(@Body examList: List<ExamRequest>) : Call<Void>

}