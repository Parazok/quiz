package com.example.quiz.model

data class AuthRequest(
    val username: String,
    val password: String
)