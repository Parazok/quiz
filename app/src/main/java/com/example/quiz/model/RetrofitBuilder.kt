package com.example.quiz.model

import com.example.quiz.utils.ResponseInterceptor
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    private const val BASE_URL = "http://test.doggydev.ir/api/"

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient())
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build() //Doesn't require the adapter
    }

    private fun okHttpClient(
    ): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(ResponseInterceptor())
            .build()
    }

    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}