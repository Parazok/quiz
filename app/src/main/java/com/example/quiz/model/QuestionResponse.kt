package com.example.quiz.model

data class QuestionResponse(
    val message: String,
    val data: List<QuestionEntity>
)