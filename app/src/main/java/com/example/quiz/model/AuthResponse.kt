package com.example.quiz.model

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    val message: String,
    @SerializedName("status_code")
    val statusCode: Int,
    val token: String
)